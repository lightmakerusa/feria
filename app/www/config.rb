preferred_syntax = :scss
sass_dir = "www/sass"
css_dir = "www/css"
images_dir = "www/images"
relative_imports = true
output_style = :compressed
