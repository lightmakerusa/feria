exec = require("child_process").exec
path = require "path"
fs = require "fs"

###
# Globals
###

###
# Global fn's
###
compileSass = ->
  console.log "Compiling sass"
  e = exec "compass compile"
  e.stdout.on "data", (data) ->
    console.log data.toString()
      
watchSass = ->
  e = exec "compass watch" 
  e.stdout.on "data", (data) ->
    console.log data
  e.stderr.on "data", (data) ->
    console.log data

###
# Grunt
###
module.exports = (grunt) ->
  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.loadNpmTasks "grunt-contrib-watch"
  grunt.loadNpmTasks "grunt-contrib-jst"
  grunt.loadNpmTasks "grunt-contrib-htmlmin"
  grunt.loadNpmTasks "grunt-contrib-uglify"
  grunt.loadNpmTasks "grunt-contrib-concat"

  grunt.initConfig
    pkg: grunt.file.readJSON "package.json"
    uglify:
      build:
        options:
          mangle: false
        expand: true
        cwd: "ios/www/js/"
        src: ["**/**/*.js"]
        dest: "ios/www/js/"
    concat:
      build:
        options:
          separator: '\n\n'
        src: "ios/www/coffee/**/*.coffee"
        dest: "ios/www/js/build.coffee"
    htmlmin:
      build:
        options:
          removeComments: true
          removeEmptyAttributes: true
          useShortDoctype: true
        expand: true
        cwd: "ios/www/book/"
        src: ["**/**/*.html"]
        dest: "ios/www/book/"
    jst:
      build:
        options:
          processName: (filename) ->
            filename.replace "ios/www/template/", ""
          namespace: "T"
          amd: true
          prettify: true
          templateSettings:
            interpolate : /\{\{(.+?)\}\}/g
            escape : /\{\{\{(.+?)\}\}\}/g
        files:
          "ios/www/js/T.js": ["ios/www/template/**/*.html"]
    coffee:
      build:
        options:
          bare: true
        files:
          "ios/www/js/build.js": "ios/www/js/build.coffee"
    watch:
      jst:
        files: ["ios/www/template/**/*.html"]
        tasks: ["jst"]
      coffee:
        files: ["ios/www/coffee/**/**/*.coffee"]
        tasks: ["concat:build", "coffee:build"]

  ###
  # Tasks
  ###
  grunt.registerTask "build", ->
    compileSass()
    grunt.task.run "jst:build"
    grunt.task.run "htmlmin:build"
    grunt.task.run "concat:build"
    grunt.task.run "coffee:build"

  grunt.registerTask "develop", ->
    grunt.task.run "build"
    watchSass()
    grunt.task.run "watch"

  grunt.registerTask "default", ["develop"]